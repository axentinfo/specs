Pod::Spec.new do |s|
  s.name         = "Box"
  s.version      = "2.0.1"
  s.summary      = "Swift µframework of the ubiquitous Box<T> & MutableBox<T> reference types, for recursive value types & misc. other purposes."
  s.description  = <<-DESC
                      Swift µframework of the ubiquitous Box<T> & MutableBox<T> reference types, for recursive value types & misc. other purposes. \n
                   DESC

  s.homepage     = "https://github.com/robrix/Box"
  s.license      = "MIT"
  s.author       = { "Rob Rix" => "rob.rix@github.com" }

  s.ios.deployment_target = "8.0"
  s.osx.deployment_target = "10.9"

  s.source       = { :git => "https://github.com/loveforgeter/Box.git", :tag => "#{s.version}" }
  s.source_files  = "Box/**/*.{h,swift}"
  s.requires_arc = true
end
